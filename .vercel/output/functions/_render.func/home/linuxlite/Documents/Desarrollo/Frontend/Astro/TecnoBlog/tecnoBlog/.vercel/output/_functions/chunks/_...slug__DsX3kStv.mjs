import { t as createAstro, m as createComponent, n as renderTemplate, o as renderComponent } from './astro/server_IoarD_mO.mjs';
import 'kleur/colors';
import { g as getCollection } from './_astro_content_ZiYUKo7k.mjs';
import { $ as $$BlogPost } from './BlogPost_BFClVzPl.mjs';

const $$Astro = createAstro("https://example.com");
async function getStaticPaths() {
  const posts = await getCollection("blog");
  return posts.map((post) => ({
    params: { slug: post.slug },
    props: post
  }));
}
const $$ = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro, $$props, $$slots);
  Astro2.self = $$;
  const post = Astro2.props;
  const { Content } = await post.render();
  return renderTemplate`${renderComponent($$result, "BlogPost", $$BlogPost, { ...post.data }, { "default": ($$result2) => renderTemplate` ${renderComponent($$result2, "Content", Content, {})} ` })}`;
}, "/home/linuxlite/Documents/Desarrollo/Frontend/Astro/TecnoBlog/tecnoBlog/src/pages/blog/[...slug].astro", void 0);

const $$file = "/home/linuxlite/Documents/Desarrollo/Frontend/Astro/TecnoBlog/tecnoBlog/src/pages/blog/[...slug].astro";
const $$url = "/blog/[...slug]";

export { $$ as default, $$file as file, getStaticPaths, $$url as url };
