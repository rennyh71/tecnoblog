import { t as createAstro, m as createComponent, n as renderTemplate, p as maybeRenderHead, s as addAttribute, z as spreadAttributes, u as renderSlot } from './astro/server_IoarD_mO.mjs';
/* empty css                                                                  */
import 'clsx';

const $$Astro = createAstro("https://example.com");
const $$HeaderLink = createComponent(($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro, $$props, $$slots);
  Astro2.self = $$HeaderLink;
  const { href, class: className, ...props } = Astro2.props;
  const { pathname } = Astro2.url;
  const subpath = pathname.match(/[^\/]+/g);
  const isActive = href === pathname || href === "/" + subpath?.[0];
  return renderTemplate`${maybeRenderHead()}<a${addAttribute(href, "href")}${addAttribute([className, { active: isActive }], "class:list")}${spreadAttributes(props)} data-astro-cid-eimmu3lg> ${renderSlot($$result, $$slots["default"])} </a> `;
}, "/home/linuxlite/Documents/Desarrollo/Frontend/Astro/TecnoBlog/tecnoBlog/src/components/HeaderLink.astro", void 0);

export { $$HeaderLink as $ };
