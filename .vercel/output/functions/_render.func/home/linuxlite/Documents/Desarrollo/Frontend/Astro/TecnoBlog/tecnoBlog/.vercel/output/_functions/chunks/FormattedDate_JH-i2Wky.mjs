import { t as createAstro, m as createComponent, n as renderTemplate, p as maybeRenderHead, s as addAttribute } from './astro/server_IoarD_mO.mjs';
import 'kleur/colors';
import 'clsx';

const $$Astro = createAstro("https://example.com");
const $$FormattedDate = createComponent(($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro, $$props, $$slots);
  Astro2.self = $$FormattedDate;
  const { date } = Astro2.props;
  return renderTemplate`${maybeRenderHead()}<time${addAttribute(date.toISOString(), "datetime")}> ${date.toLocaleDateString("en-us", {
    year: "numeric",
    month: "short",
    day: "numeric"
  })} </time>`;
}, "/home/linuxlite/Documents/Desarrollo/Frontend/Astro/TecnoBlog/tecnoBlog/src/components/FormattedDate.astro", void 0);

export { $$FormattedDate as $ };
